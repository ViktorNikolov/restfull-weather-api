Searching for today weather of a specific city. -> I should be able to search for today's weather by inputting the city name.

Saveing weather data. -> I should be able to save weather data for retrieval.

Getting historical weather data. -> I should be able to look for weather data from past periods.

Deleting historical weather data. -> I should be able to delete an existing weather record.

Updating historical weather data. -> I should be able to update an existing weather record.

