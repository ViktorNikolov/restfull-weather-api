package com.oddle.app.weather.controller;

import com.oddle.app.weather.model.Weather;
import com.oddle.app.weather.service.WeatherService;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.Collections;
import java.util.List;
import java.util.Map;

@RestController
public class WeatherController {

    private final WeatherService weatherService;

    public WeatherController(WeatherService weatherService) {
        this.weatherService = weatherService;
    }

    @GetMapping("")
    public Map<String, Object> getWeathers() {
        return Collections.singletonMap("message", "Welcome to Oddle Backend Challenge");
    }

    @GetMapping("/{cityName}")
    public Map<String, Object> getByNameFromApi(@PathVariable String cityName) {
        return weatherService.getByNameFromApi(cityName);
    }

    @GetMapping("/data")
    public List<Weather> getAllWeathers() {
        return weatherService.getAllWeathersFromDataBase();
    }

    @PostMapping("/{cityName}")
    public void createInDataBase(Weather weather, @PathVariable String cityName) {
        String date = LocalDate.now().toString();
        weatherService.createInDataBase(weather, cityName, date);
    }

    @GetMapping("/data/{cityName}")
    public List<Weather> getByCity(@PathVariable String cityName) {
        return weatherService.getByCityFromDataBase(cityName);
    }

    @PutMapping("/data/")
    public void updateInDataBase(@RequestBody Weather weather) {
        weatherService.updateInDataBase(weather);
    }

    @DeleteMapping("/data/id/{id}")
    public void deleteInDataBaseById(@PathVariable int id) {
        weatherService.deleteInDataBaseById(id);
    }

    @DeleteMapping("/data/{cityName}")
    public void deleteInDataBaseByCityName(@PathVariable String cityName) {
        weatherService.deleteInDataBaseByCityName(cityName);
    }

}