package com.oddle.app.weather.service;

import com.oddle.app.weather.model.Weather;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import javax.persistence.EntityNotFoundException;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

@Service
public class WeatherServiceImpl implements WeatherService{
    private static final String API_KEY = "e1a3da9115100c719503922681176d6c";

    private final SessionFactory sessionFactory;

    public WeatherServiceImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }


    @Override
    public Map<String, Object> getByNameFromApi(String cityName) {
        String result;

        try {
            URL url = new URL("http://api.openweathermap.org/data/2.5/weather?q="
                    + cityName + "&appid=" + API_KEY + "&units=metric");
            URLConnection conn = url.openConnection();
            BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            result = rd.readLine();
            rd.close();
        } catch (IOException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
        return Collections.singletonMap(cityName, result);
    }

    @Override
    public List<Weather> getAllWeathersFromDataBase() {
                try (Session session = sessionFactory.openSession()) {
            Query<Weather> query = session.createQuery("from Weather", Weather.class);
            return query.list();
        }
    }

    @Override
    public void createInDataBase(Weather weather, String cityName, String date) {
        getByNameFromApi(cityName);
        weather.setWeatherJson(getByNameFromApi(cityName).get(cityName).toString());
        weather.setCityName(cityName);
        weather.setDate(date);
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.save(weather);
            session.getTransaction().commit();
        }
    }

    @Override
    public List<Weather> getByCityFromDataBase(String cityName) {
        try (Session session = sessionFactory.openSession()) {
            Query<Weather> query = session.createQuery(
                    "from Weather where cityName=:cityName order by date", Weather.class);
            query.setParameter("cityName", cityName);

            if (query.list().size() == 0 || cityName == null) {
                return new ArrayList<>();
            }
            return query.list();
        }
    }

    @Override
    public Weather getByIdFromDataBase(int id) {
        try (Session session = sessionFactory.openSession()) {
            Weather weather = session.get(Weather.class, id);
            if (weather == null) {
                throw new EntityNotFoundException(
                        String.format("Weather with id %d not found!", id));
            }
            return weather;
        }
    }

    @Override
    public void updateInDataBase(Weather weather) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(weather);
            session.getTransaction().commit();
        }
    }

    @Override
    public void deleteInDataBaseByCityName(String cityName) {
        List<Weather> weathers = getByCityFromDataBase(cityName);

        try (Session session = sessionFactory.openSession()) {
            for (Weather weather : weathers) {
                session.beginTransaction();
                session.delete(weather);
                session.getTransaction().commit();
            }
        }
    }

    @Override
    public void deleteInDataBaseById(int id) {
        Weather weather = getByIdFromDataBase(id);

        try (Session session = sessionFactory.openSession()) {
                session.beginTransaction();
                session.delete(weather);
                session.getTransaction().commit();
        }
    }

}