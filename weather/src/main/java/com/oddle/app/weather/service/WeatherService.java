package com.oddle.app.weather.service;

import com.oddle.app.weather.model.Weather;

import java.util.List;
import java.util.Map;

public interface WeatherService {

    Map<String, Object> getByNameFromApi(String cityName);

    List<Weather> getAllWeathersFromDataBase();

    void createInDataBase(Weather weather, String cityName, String date);

    List<Weather> getByCityFromDataBase(String cityName);

    Weather getByIdFromDataBase(int id);

    void updateInDataBase(Weather weather);

    void deleteInDataBaseByCityName(String cityName);

    void deleteInDataBaseById(int id);
}
